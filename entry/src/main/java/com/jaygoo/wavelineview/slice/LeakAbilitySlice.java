package com.jaygoo.wavelineview.slice;

import com.jaygoo.wavelineview.MyApplication;
import com.jaygoo.wavelineview.ResourceTable;
import jaygoo.widget.wlv.WaveLineView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.bundle.AbilityInfo;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import static ohos.bundle.AbilityInfo.DisplayOrientation.LANDSCAPE;
import static ohos.bundle.AbilityInfo.DisplayOrientation.PORTRAIT;

public class LeakAbilitySlice extends AbilitySlice {
    private static final int LABELNO = 0x000110;
    private static final int TWO = 2;
    private static final int ONE = 1;
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, LABELNO, "CircleImageView");
    private static HiLogLabel label1 = new HiLogLabel(HiLog.LOG_APP, LABELNO, "mlvMainAbility");
    private WaveLineView waveLineView;

    private Button startBtn;
    private Button stopBtn;
    private Button leakTestBtn;
    private DirectionalLayout titlebardirLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_leak);
        getWindow().setStatusBarColor(Color.getIntColor("#f34050B5"));
        int width = getDisplayWidthInPx(getContext());
        int height = getDisplayHeightInPx(getContext());
        titlebardirLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_titlebar_dirLayout_leak);
        waveLineView = (WaveLineView) findComponentById(ResourceTable.Id_waveLine_leak);
        if (MyApplication.getScreenOrientation() == TWO) {
            HiLog.info(label1,"ScreenOrientation  width: " + width + ", height: " + height);
            int mWidth;
            if (width < height) {
                mWidth = height;
            } else {
                mWidth = width;
            }
            waveLineView.setWidth(mWidth);
        }

        if (MyApplication.getScreenOrientation() == ONE) {
            HiLog.info(label1,"ScreenOrientation  ver width: " + width + ", height: " + height);
            waveLineView.setWidth(width);
        }

        startBtn = (Button) findComponentById(ResourceTable.Id_button_startBtn_leak);
        stopBtn = (Button) findComponentById(ResourceTable.Id_stopBtn_leak);
        leakTestBtn = (Button) findComponentById(ResourceTable.Id_leakTestBtn_leak);
        leakTestBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                release();
            }
        });
        startBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                startBtn();
            }
        });
        stopBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                stopBtn();
            }
        });
    }

    private void startBtn() {
        if (waveLineView != null) {
            waveLineView.setVisibility(Component.VISIBLE);
            waveLineView.startAnim();
            MyApplication.setWaveStart(true);
        } else {
            HiLog.info(label, "waveLineView   null ");
            stopBtn.setText("I am stop Button");
        }
    }

    private void stopBtn() {
        if (waveLineView.isRunning()) {
            waveLineView.stopAnim();
            MyApplication.setWaveStart(false);
            MyApplication.setOrientationChanged(false);
        }
    }

    private void release() {
        titlebardirLayout.setVisibility(Component.VISIBLE);
        if (waveLineView.isRunning()) {
            waveLineView.release();
        }
        waveLineView.setVisibility(Component.INVISIBLE);
        MyApplication.setWaveStart(false);
        MyApplication.setOrientationChanged(false);
        MainAbilitySlice mainAbilitySlice = new MainAbilitySlice();
        Intent intent1 = new Intent();
        present(mainAbilitySlice,intent1);
        onStop();
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        MyApplication.setOrientationChanged(true);
        int height = getDisplayHeightInPx(getContext());
        int width = getDisplayWidthInPx(getContext());
        HiLog.info(label,"" + width + ",  " + height);
        if (waveLineView != null) {
            waveLineView.setWidth(width);
            if (displayOrientation == LANDSCAPE) {
                HiLog.warn(label,"======LeakAbility onOrientationChanged");

                if (waveLineView.isRunning()) {
                    waveLineView.stopAnim();
                }
                if (MyApplication.isWaveStart() == true) {
                    waveLineView.startAnim();
                }

                if (waveLineView.isRunning() == false) {
                    waveLineView.startAnim();
                    waveLineView.stopAnim();
                }

            }

            else if (displayOrientation == PORTRAIT) {
                HiLog.warn(label,"======LeakAbility onOrientationChanged  111111");

                if (waveLineView.isRunning()) {
                    waveLineView.stopAnim();
                }
                if (MyApplication.isWaveStart() == true) {
                    waveLineView.startAnim();
                }
                if (waveLineView.isRunning() == false) {
                    waveLineView.startAnim();
                    waveLineView.stopAnim();
                }
            }
        }
    }

    /**
     * 获取屏幕宽度
     *
     * @param context

     * @return 屏幕宽度
     *
     */
    public static int getDisplayWidthInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().width;
    }

    /**
     * 获取屏幕高度，不包含状态栏的高度
     *
     * @param context
     *
     * @return 屏幕高度，不包含状态栏的高度
     */

    public static int getDisplayHeightInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().height;
    }

    @Override
    protected void onStop() {
        super.onStop();
        HiLog.info(label,"onStop");
        MyApplication.setWaveStart(false);
        MyApplication.setOrientationChanged(false);
        terminate();
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        HiLog.info(label,"onAbilityResult");
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        HiLog.info(label,"onBackground");
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        HiLog.info(label,"onInactive");
    }

    @Override
    public void onActive() {
        super.onActive();
        HiLog.info(label,"onActive");
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        HiLog.info(label,"onForeground");
    }
}
