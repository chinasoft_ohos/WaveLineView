/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain an copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



package com.jaygoo.wavelineview;

import ohos.aafwk.ability.AbilityPackage;
/**
 *
 * 记录一些状态
 *
 * @author JayGoo
 * @since 2017-07-21
 *
 * **/
public class MyApplication extends AbilityPackage {
    private static boolean waveStart;

    // 方向变化
    private static boolean orientationChanged;

    // 2代表横屏, 1代表竖屏
    private static int screenOrientation;

    public static int getScreenOrientation() {
        return screenOrientation;
    }

    public static void setScreenOrientation(int screenOrientation1) {
        screenOrientation = screenOrientation1;
    }

    public static boolean isOrientationChanged() {
        return orientationChanged;
    }

    public static void setOrientationChanged(boolean orientationChanged1) {
        orientationChanged = orientationChanged1;
    }

    public static boolean isWaveStart() {
        return waveStart;
    }

    public static void setWaveStart(boolean waveStart1) {
        waveStart = waveStart1;
    }
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
