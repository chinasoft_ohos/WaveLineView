package com.jaygoo.wavelineview;

import com.jaygoo.wavelineview.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.AbilityInfo;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbility extends Ability {
    private static final int LABELNO = 0x000110;
    private static final int TWO = 2;
    private static final int ONE = 1;
    private static HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, LABELNO, "mlvMainAbility");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        HiLog.info(label,"OrientationChanged..  " + displayOrientation.name());
        if (displayOrientation.name().equals("PORTRAIT")) {
            MyApplication.setScreenOrientation(ONE);
        } else if (displayOrientation.name().equals("LANDSCAPE")) {
            MyApplication.setScreenOrientation(TWO);
        }
    }
}
