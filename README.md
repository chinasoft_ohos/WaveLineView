# WaveLineView



### 项目介绍
- 项目名称：WaveLineView
- 所属系列：openharmony第三方组件适配移植
- 功能：分段器自定义组件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release v1.0.4

### 效果演示

![sc1](pictures/sc1.gif)

### 安装教程

在moudle级别下的build.gradle文件中添加依赖
 ```gradle
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}

// 添加依赖库
dependencies {
    implementation 'com.gitee.chinasoft_ohos:wavelineview:1.0.0'   
}
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


### 使用说明
属性

| backgroundColor        | color    | 背景色  |
| --------   | -----:   | :----: |
| wlvLineColor        | color      |   波浪线的颜色    |
| wlvThickLineWidth        | dimension      |   中间粗波浪曲线的宽度    |
| wlvFineLineWidth        | dimension      |   三条细波浪曲线的宽度    |
| wlvMoveSpeed|float|波浪线移动的速度，默认值为290F，方向从左向右，你可以使用负数改变移动方向|
|wlvSamplingSize|integer|采样率，动画效果越大越精细，默认64|
|wlvSensibility|integer|灵敏度，范围[1,10]，越大越灵敏，默认值为5

```xml
 <jaygoo.widget.wlv.WaveLineView
        ohos:id="$+id:waveLine"
        ohos:width="380fp"
        ohos:height="180fp"
        app:wlvBackgroundColor = "#992222"
        app:wlvSamplingSize="64"
        app:wlvLineColor="#2ede84"
        app:wlvThickLineWidth="6"
        app:wlvFineLineWidth="2"
        app:wlvMoveSpeed="250"
        app:wlvSensibility="5"
        />
```
* Java 调用示例

```java
        //启动动画
        waveLineView.startAnim();
        //停止动画
        waveLineView.stopAnim();
```


### 测试信息
CodeCheck代码测试无异常  

CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异  

### 版本迭代
- 1.0.0 
